import processing.serial.*;

Serial PX4;

int x;
float y_ant;

//PFont fonte;

void setup() {
  size(displayWidth, displayHeight);

  PX4 = new Serial(this, "/dev/ttyACM0", 921600);

  //fonte = createFont("Courier10PitchBT-Roman", 24);
  //textFont(fonte);
  //textAlign(RIGHT, CENTER);

  reset();

  PX4.clear();

  frameRate(120);
}

void draw() {

  if (PX4.available () > 0 && true) {
    String dado = PX4.readStringUntil('\n');
    //println("Dado: ", dado);

    if (dado != null) {

      float y = float(dado);

      println("Y: ", y);

      if (y <= 255) {
        if (x != 101) {
          //line(x-1, 930 - y_ant * 100, x++, 930 - y * 100);
          stroke(245, 58, 135);
          line(x-1, 800 - y_ant * 100, x++, 800 - y * 100);
        }

        y_ant = y;

        if (x++ > width - 100) 
          reset();
      }
    }
  }
}

boolean sketchFullScreen() {
  return true;
}

void reset() {
  x = 101;

  background(54);

  strokeWeight(3);
  stroke(168);
  line(100, 930, width - 100, 930);
  line(100, 930, 100, 250);
}

